Textmate 2 Bundle for handling whitespace automagically

- strip trailing whitespace
- ensure there's a newline at EOF
- ensure there's only ONE newline at EOF

~~Shamelessly lifted from~~ Heavily inspired by:

- https://github.com/bomberstudios/Strip-Whitespace-On-Save.tmbundle
- https://github.com/hajder/Ensure-New-Line-at-the-EOF.tmbundle

Installation instructions

1. `mkdir -p ~/Library/Application Support/Avian/Bundles`
2. `cd ~/Library/Application Support/Avian/Bundles`
3. `git clone https://gitlab.devfu.com/bm5k/tm2-whitespace.tmbundle.git`
4. Profit
